
public class MainClass {
	
	public static void main(String[] args) {
		SmartHouse smartHouse= new SmartHouse();
		smartHouse.readMonitoredData("Activities.txt");
		//System.out.println(smartHouse);
		
		System.out.println("Number of different dates: " + smartHouse.countDistinctDates());
		smartHouse.actionFrequency();
		smartHouse.dailyActionFrequency();
		smartHouse.totalActivityDurations();
		smartHouse.shortActivities();
	}	
}
