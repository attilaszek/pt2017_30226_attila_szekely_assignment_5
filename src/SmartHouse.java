import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.PeriodFormat;

import javax.swing.JOptionPane;

import java.util.Map;
import java.util.function.Function;

public class SmartHouse {
	private List<MonitoredData> monitoredData = new ArrayList<MonitoredData>();

	public SmartHouse() {
		
	}
	
	@SuppressWarnings("resource")
	public void readMonitoredData(String path) {
		FileReader inputReader;
		BufferedReader in;
		String buffer;
		String[] data;
		Date startTime;
		Date endTime;
		
		try {
			inputReader = new FileReader(path);
			in = new BufferedReader(inputReader);
			 
			while ((buffer = in.readLine()) != null) {
				data = buffer.split("\\s+");
				if (data.length!=5) throw new Exception("Invalid input file format");
				startTime = MonitoredData.simpleDateFormat.parse(data[0]+" "+data[1]);
				endTime = MonitoredData.simpleDateFormat.parse(data[2]+" "+data[3]);			 
				monitoredData.add(new MonitoredData(startTime, endTime, data[4]));
			}
			in.close();
			inputReader.close();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "File not found");
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Input/output exception");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
		}
	}
	

	public int countDistinctDates() {
		int returnValue = monitoredData
									.stream()
									.map(MonitoredData::getStartDate)
									.distinct()
									.collect(Collectors.toCollection(TreeSet::new))
									.size();
		return returnValue;
	}
	
	public void actionFrequency() {
		Map<String, Long> map = 
				 monitoredData	.stream()
						.map(x -> x.getActivityLabel())
						.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		
		try {
			PrintWriter writer= new PrintWriter("02_actionFrequency.txt");
			for (String key : map.keySet()) {
				writer.println(key+" "+map.get(key));
			}
			writer.close();
		} catch (FileNotFoundException e1) {

		}
	}
	
	public void dailyActionFrequency() {
		AtomicInteger ai = new AtomicInteger();
		Comparator<MonitoredData> byDate = (e1, e2) -> Long.compare(e1.getStartTime().getTime(), e2.getStartTime().getTime());
		
		Map<Integer, Map<String, Long>> map = 
				monitoredData	.stream()
								.sorted(byDate)
								.collect(Collectors.groupingBy(MonitoredData::getStartDate, Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting())))
								.entrySet()
								.stream()
								.sorted(Map.Entry.<String, Map<String, Long>>comparingByKey())
								.collect(Collectors.toMap(e -> ai.getAndIncrement(), e -> e.getValue()));

		try {
			PrintWriter writer= new PrintWriter("03_dailyActionFrequency.txt");
			for (Integer key : map.keySet()) {
				writer.println("Day "+key);
				for (String activity : map.get(key).keySet()) {
					writer.println("   "+activity+": "+map.get(key).get(activity));
				}
			}
			writer.close();
		} catch (FileNotFoundException e1) {

		}	
	}
	
	public void totalActivityDurations() {
		Map<String, Duration> map=
				monitoredData	.stream()
								.collect(Collectors.groupingBy(o -> o.getActivityLabel(), Collectors.summingLong(o -> o.getEndTime().getTime() - o.getStartTime().getTime())))
								.entrySet()
								.stream()
								.filter(e -> e.getValue()>10*60*60*1000)
								.collect(Collectors.toMap(e -> e.getKey(), e -> new Duration(e.getValue())));
	
		try {
			PrintWriter writer= new PrintWriter("04_totalActivityDurations.txt");
			for (String activity : map.keySet()) {
				Duration duration = map.get(activity);
				writer.printf("%-15s %s\n", activity, PeriodFormat.getDefault().print(duration.toPeriod()));
			}
			writer.close();
		} catch (FileNotFoundException e1) {

		}	
	}
	
	public void shortActivities() {
		Map<String, Long> mapShort = 
				monitoredData	.stream()
								.filter(o -> (new Duration(new DateTime(o.getStartTime()), new DateTime(o.getEndTime())).getStandardMinutes())<=5)
								.collect(Collectors.groupingBy(o -> o.getActivityLabel(), Collectors.counting()));
		Map<String, Long> mapAll = 
				monitoredData	.stream()
								.collect(Collectors.groupingBy(o -> o.getActivityLabel(), Collectors.counting()));
		List<String> list = 
				mapShort.entrySet()	.stream()
									.filter(e -> e.getValue() / (float)mapAll.get(e.getKey()) > 0.9)
									.map(e -> e.getKey())
									.collect(Collectors.toList());
		
		try {
			PrintWriter writer= new PrintWriter("05_shortActivities.txt");
			for (String activity : list) {
				writer.println(activity);
			}
			writer.close();
		} catch (FileNotFoundException e1) {

		}	
	}
	
	public String toString() {
		String toReturn="";
		for (MonitoredData data : monitoredData) {
			toReturn += data.toString() + "\n";
		}
		return toReturn;
	}
}
