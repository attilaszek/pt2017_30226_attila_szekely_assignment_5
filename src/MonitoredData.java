import java.text.SimpleDateFormat;
import java.util.Date;

public class MonitoredData {
	public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
	public static SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd");
	
	private Date startTime ;
	private Date endTime;
	private String activityLabel;
	
	public MonitoredData(Date startTime, Date endTime, String activityLabel) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.activityLabel = activityLabel;
	}
	
	public Date getStartTime() {
		return startTime;
	}
	
	public String getStartDate() {
		return dateFormat.format(startTime);
	}
	
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	public Date getEndTime() {
		return endTime;
	}
	
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	public String getActivityLabel() {
		return activityLabel;
	}
	
	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}
	
	public String toString() {
		return simpleDateFormat.format(startTime) + " " + simpleDateFormat.format(endTime) + " " + activityLabel;
	}
}
